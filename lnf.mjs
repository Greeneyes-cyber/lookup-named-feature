import isPromise from 'p-is-promise'
import deepLookup from 'lodash.get'
import vTry from 'vtry'


function callOptToArgs(x) {
  if (x === true) { return [] }
  if (Array.isArray(x)) { return x }
  return [x]
}


function assertJsType(want, x) {
  if (!want) { return }
  const t = typeof x
  if (t === want) { return }
  throw new TypeError(`Expected a ${want} but found ${t} ${x}`)
}


function refine(categ, name, found, opt) {
  if (isPromise(found)) {
    if (opt.annotatePr !== false) {
      return refine(categ, name,
        vTry.pr(1, `${categ} ${name}`)(found),
        { ...opt, annotatePr: false })
    }
    return found.then(x => refine(categ, name, x, opt))
  }
  if (opt.dive) {
    return refine(categ, name,
      deepLookup(found, opt.dive),
      { ...opt, dive: false })
  }
  if (opt.call !== undefined) {
    if (typeof found !== 'function') {
      throw new TypeError(`Found a non-function, cannot call that: ${found}`)
    }
    return refine(categ, name,
      found(...callOptToArgs(opt.call)),
      { ...opt, call: undefined })
  }
  assertJsType(opt.wantJsType, found)
  return found
}


function lookupNamedFeature(features, key, origOpt) {
  const opt = (origOpt || false)
  let category = (opt.category || 'feature')
  let name = key
  if (opt.cfg) {
    category = key
    name = opt.cfg[key]
  }
  if (!features) {
    throw new TypeError(`Cannot lookup ${category} ${
      name} in non-object "${features}"`)
  }
  const found = features[name]
  if ((found === null) || (found === undefined)) {
    throw new Error(`Unsupported ${category}: ${name}`)
  }
  return vTry(refine, `${category} ${name}`)(category, name, found, opt)
}


export default lookupNamedFeature
